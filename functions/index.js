const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);   

exports.sendNotification = functions.database
    .ref('/messages/{roomId}/{messageKey}')
    .onCreate(event => {
        const dataSnapshot = event.data;
        // Notification payload
        const text = dataSnapshot.val().text
        const payload = {
            notification: {
                title: `${dataSnapshot.val().sender_name} send a message`,
                body: text ? (text.lenght <= 100 ? text : text.substring(0, 97) + '...') : ''
            }
        };

        const roomId = event.params.roomId
        const senderId = dataSnapshot.val().sender_id
        return admin.database().ref('rooms').child(roomId).once('value').then(snapshot => {
            const receiverIds = Object.keys(snapshot.val())
                .filter(id => String(id) !== String(senderId))
            const notificationToSend = [];
            receiverIds.forEach(receiverId => {
                notificationToSend.push(
                    admin.database().ref('fcmTokens').child(receiverId).once('value').then(snapshot => {
                        if(snapshot.val()) {
                            const tokens = Object.keys(snapshot.val())
            
                            return admin.messaging().sendToDevice(tokens, payload).then(response => {
                                const tokenToRemove = []
            
                                response.results.forEach((result, index) => {
                                    const error = result.error
                                    if (error){
                                        console.log("Error sending notification to device token : ", tokens[index], error)
            
                                        if(error.code === 'messaging/invalid-registration-token' || error.code === 'messaging/registration-token-not-registered'){
                                            tokenToRemove.push(snapshot.ref.child(tokens[index]).remove())
                                        }
                                    }
                                })
            
                                return Promise.all(tokenToRemove)
                            })
                        } else {
                            return console.log("No tokens to send notifications")
                        }
                    })
                )
            });
            return Promise.all(notificationToSend)
        })
    });